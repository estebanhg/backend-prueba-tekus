const express=require('express')
const app=express() //funciones express
const morgan = require('morgan')
const cors = require('cors')

// setings
app.set('Port',4000)

//Midedlewares
app.use(morgan('dev'))

// Configurar cabeceras y cors
app.use(cors())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


// Routes
app.use('/', require('./routes/product.route'));

//Start
app.listen(app.get('Port'),()=>{
    console.log('servidor conectado puerto: ',app.get('Port'))
})