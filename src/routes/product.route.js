const {Router} = require('express');
const router = Router();

const ProductsCtrl = require('../controllers/Product.controller')


router.get('/products',ProductsCtrl.products)

module.exports = router;
